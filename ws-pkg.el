(define-package "ws" "0.0.1"
  "Workspaces for org-roam"
  '((taxy "0.9")
    (taxy-magit-section "0.9.1")
    (org-roam "20220613.2301")))
