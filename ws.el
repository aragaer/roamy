;;; ws.el --- workspaces for org-roam  -*- lexical-binding: t; -*-

;; Author: aragaer <aragaer@gmail.com>
;; Version: 0.1.0
;; Package-Requires: ((taxy "0.9") (taxy-magit-section "0.9.1") (org-roam))
;; Keywords: roam, workspace

;;; Commentary:

(require 'taxy)
(require 'taxy-magit-section)
(require 'org-roam)

;;; org roam context tags
(if (not (fboundp 'org-roam-node-context-tag))
  (cl-defmethod org-roam-node-context-tag ((node org-roam-node))
    "Return the @tags of NODE."
    (cl-find ?@ (org-roam-node-tags node)
             :key #'string-to-char
             :test #'char-equal)))

(cl-defstruct (roamy-node)
  id title ctx)

(defun node-fields (node)
  (mapcan (lambda (item)
            (list (car item) (funcall (cdr item) node)))
          '((:id . org-roam-node-id)
            (:title . org-roam-node-title)
            (:ctx . org-roam-node-context-tag))))

(defun roam-node->roamy-node (node)
  (apply #'make-roamy-node (node-fields node)))

(cl-defstruct (workspace
               (:include roamy-node)))

(defun roam-node->workspace (node)
  (apply #'make-workspace (node-fields node)))

(defvar roamy-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "RET") #'roamy-RET)
    (define-key map [mouse-1] #'roamy-mouse-1)
    map))

(defgroup roamy nil
  "Roamy settings"
  :group 'org-roam)

(defcustom roamy-context-tree nil
  "Tree-like structure describing hierarchy of contexts"
  :group 'roamy
  :type '(alist :key-type string))

(defvar-local roamy-nodes-fn nil
  "Function to retrieve nodes shown in the current Roamy buffer.")

(defvar-local roamy-title nil
  "Root node title of the current Roamy buffer.")

(defun roamy-mouse-1 (event)
  "Call `roamy-RET' with point at EVENT's position."
  (interactive "e")
  (mouse-set-point event)
  (call-interactively #'roamy-RET))

(defun roamy-RET ()
  "Go to form at point, or expand section at point."
  (interactive)
  (cl-etypecase (oref (magit-current-section) value)
    (roamy-node (roamy-jump (oref (magit-current-section) value)))
    (taxy-magit-section (call-interactively #'magit-section-cycle))
    (null nil)))

(defun roamy-revert (_ignore-auto _noconfirm)
  "Revert current Deffy buffer."
  (interactive)
  (roamy roamy-title roamy-nodes-fn))

(defun roamy-jump (node)
  (org-roam-node-open (org-roam-node-from-id (roamy-node-id node))))

(define-derived-mode roamy-mode magit-section-mode "Roamy"
  :global nil
  (setq-local revert-buffer-function #'roamy-revert))

;;;; Functions
(defun find-tree-path (tree item)
  (if (assoc-string item tree)
      (list item)
    (cl-loop for sub-tree in tree
             for path = (find-tree-path sub-tree item)
             if path
             return (cons (car sub-tree) path))))

(defun constantly (result)
  (lambda (_) result))

(defun roamy--build-taxy (title nodes)
  (cl-labels ((format-node
               (node)
               (roamy-node-title node))
              (make-node
               (&rest args)
               (apply #'make-taxy-magit-section
                      :make #'make-node
                      :format-fn #'format-node
                      args)))
    (let ((taxy
            (make-taxy-magit-section
             :name title
             :take (lambda (item taxy)
                     (let* ((ctx (roamy-node-ctx item))
                            (chain (if ctx
                                       (or
                                        (find-tree-path roamy-context-tree ctx)
                                        (list "Self" ctx))
                                     (list "Self" "Other"))))
                       (taxy-take-keyed
                         (mapcar #'constantly chain)
                         item taxy)))
             :make #'make-node
             :format-fn #'format-node)))
      (taxy-fill nodes taxy))))

;;;###autoload
(defun roamy (title nodes-fn &optional make-fn)
  (cl-labels ((node-to-taxy-item
               (node)
               (funcall (or make-fn 'roam-node->roamy-node) node)))
    (let ((buffer-name (format "*Roamy: %s*" title))
          (items (mapcar #'node-to-taxy-item (funcall nodes-fn))))
      (with-current-buffer (get-buffer-create buffer-name)
        (roamy-mode)
        (setq-local roamy-nodes-fn nodes-fn
                    ;;; FIXME: not setting root section to nil slows everything down
                    ;;; it should not
                    magit-root-section nil
                    roamy-title title)
        (let ((taxy (roamy--build-taxy title items))
              (inhibit-read-only t))
          (delete-all-overlays)
          (erase-buffer)
          (save-excursion
            (taxy-magit-section-insert taxy :items 'last)))
        (let ((display-buffer-alist `((,buffer-name display-buffer-same-window))))
          (pop-to-buffer (current-buffer)))))))

;;;###autoload
(defun workspacy (&optional arg)
  (interactive "P")
  (let ((tag (if arg "#archived" "#workspace")))
    (roamy (if arg "Archived workspaces" "Workspaces")
           (lambda ()
             (cl-loop for (id) in (org-roam-db-query
                                   [:select node-id
                                    :from tags
                                    :where (= tag $s1)]
                                   tag)
                      collect (org-roam-node-from-id id)))
           'roam-node->workspace)))

(provide 'roamy)
(provide 'workspacy)

;;; ws.el ends here
